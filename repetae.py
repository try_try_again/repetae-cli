#!/bin/python3

"""
        DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                    Version 2, December 2004

 Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>

 Everyone is permitted to copy and distribute verbatim or modified
 copies of this license document, and changing it is allowed as long
 as the name is changed.

            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

  0. You just DO WHAT THE FUCK YOU WANT TO.
"""

from os.path import exists
import os
import sys
import json
import datetime
import time
import random

__COLLECTION_FILE__ = "cards.json"
__TIME_INTERVALS__ = {
        0: datetime.timedelta(seconds=0).total_seconds(),
        1: datetime.timedelta(seconds=5).total_seconds(),
        2: datetime.timedelta(seconds=25).total_seconds(),
        3: datetime.timedelta(minutes=2).total_seconds(),
        4: datetime.timedelta(minutes=10).total_seconds(),
        5: datetime.timedelta(hours=1).total_seconds(),
        6: datetime.timedelta(hours=5).total_seconds(),
        7: datetime.timedelta(days=1).total_seconds(),
        8: datetime.timedelta(days=5).total_seconds(),
        9: datetime.timedelta(days=25).total_seconds(),
        10: datetime.timedelta(days=120).total_seconds(),
        11: datetime.timedelta(days=730).total_seconds(),
        }

def read_from_json_file(filename):
    with open(filename) as collection_file:
        collection = json.load(collection_file)
    return collection

def write_to_json_file(filename, collection):
    with open(filename, "w") as outfile:
        outfile.write(json.dumps(collection, indent=2))

def check_for_existing_card(collection, question):

    for card in collection:
        if card['question'] == question:
            return True
        else:
            continue

    return False

def get_current_timestamp():
    return time.mktime(datetime.datetime.now().timetuple())

def increment_box(num):
    return num + 1 if num < 11 else num

def decrement_box(num):
    return num - 1 if num > 0 else num

def get_next_study_time(collection):
    timestamp = sorted(collection, key = lambda x: x['date'])[0]['date']
    datetime_obj = datetime.datetime.fromtimestamp(int(timestamp))
    readable_date = datetime_obj.strftime("%d.%m.%y %H:%M:%S")
    return readable_date

def get_due_cards(collection):
    due_cards =  list(filter(lambda x: x['date'] < get_current_timestamp(), collection))
    if len(due_cards) == 0:
        next_study_time = get_next_study_time(collection)
        print("ALL DONE FOR NOW!")
        print(f"Next study time is at: {next_study_time}")
        exit()
    else:
        due_cards.sort(key = lambda x: x['date'])
        due_cards.sort(key = lambda x: x['date'] == 0)
        return due_cards

def review_incorrect(card):
        print("Wrong!")

        while True:
            print("Please type in correct answer:")
            print(f"\"{card['answer']}\"")
            if card['answer'] == input():
                break
            else:
                continue

def game_loop(collection):

    while True:
        os.system('cls' if os.name == 'nt' else 'clear')
        due_cards = get_due_cards(collection)
        current_card = due_cards[0]
        print(f"There are {len(due_cards)} cards left.")
        print(current_card['question'])
        response = input()
        if response == current_card['answer']:
            current_card['box'] = increment_box(current_card['box'])
        else:
            review_incorrect(current_card)
            current_card['box'] = decrement_box(current_card['box'])

        current_card['date'] = get_current_timestamp() + __TIME_INTERVALS__[current_card['box']]
        write_to_json_file(__COLLECTION_FILE__, collection)

def check_for_duplicate_cards(collection):
    questions = list(map(lambda x: x['question'], collection))
    duplicates = set([question for question in questions if questions.count(question) > 1])
    if len(duplicates) > 0:
        print("DUPLICATE QUESTIONS DETECTED!")
        print("The following duplicates were found:")
        for duplicate in duplicates:
            print(f"\t\"{duplicate}\"")
        exit()

def check_collection_is_valid(collection):
    for card in collection:
        try:
            int(card['date'])
            int(card['box'])
            card['question']
            card['answer']
        except:
            print(f"Malformed Card: {card}")
            exit()

if __name__ == "__main__":
    if not exists(__COLLECTION_FILE__):
        print("ERROR!")
        print(f"File: \"{__COLLECTION_FILE__}\" not found!")
    else:
        collection = read_from_json_file(__COLLECTION_FILE__)
        check_collection_is_valid(collection)
        check_for_duplicate_cards(collection)
        try:
            game_loop(collection)
        except KeyboardInterrupt:
            print("\nCTRL+C detected. Goodbye!")
            exit()
