y k u   7 8 9   Move commands:
 \|/     \|/            yuhjklbn: go one step in specified direction
h-.-l   4-.-6           YUHJKLBN: go in specified direction until you
 /|\     /|\                        hit a wall or run into something
b j n   1 2 3           g<dir>:   run in direction <dir> until something
      numberpad                     interesting is seen
                        G<dir>,   same, except a branching corridor isn't
 <  up                  ^<dir>:     considered interesting (the ^ in this
                                    case means the Control key, not a caret)
 >  down                m<dir>:   move without picking up objects/fighting
                        F<dir>:   fight even if you don't sense a monster
                If the number_pad option is set, the number keys move instead.
                Depending on the platform, Shift number (on the numberpad),
                Meta number, or Alt number will invoke the YUHJKLBN commands.
                Control <dir> may or may not work when number_pad is enabled,
                depending on the platform's capabilities.
                Digit '5' acts as 'G' prefix, unless number_pad is set to 2
                in which case it acts as 'g' instead.
                If number_pad is set to 3, the roles of 1,2,3 and 7,8,9 are
                reversed; when set to 4, behaves same as 3 combined with 2.
                If number_pad is set to -1, alphabetic movement commands are
                used but 'y' and 'z' are swapped.

General commands:
{
    "answer": "?",
     "question": "in nethack: display one of several informative texts",
    "date": 0,
    "box": 0
},
{
    "answer": "#",
     "question": "in nethack: quit end the game without saving current game",
    "date": 0,
    "box": 0
},
{
    "answer": "S",
     "question": "in nethack: save the game (to be continued later) and exit",
    "date": 0,
    "box": 0
},
{
    "answer": "!",
     "question": "in nethack: escape to some SHELL (if allowed; 'exit' to resume play)",
    "date": 0,
    "box": 0
},
{
    "answer": "^Z",
     "question": "in nethack: suspend the game (independent of your current suspend char)",
    "date": 0,
    "box": 0
},
{
    "answer": "fg",
     "question": "in nethack: resume game",
    "date": 0,
    "box": 0
},
{
    "answer": "O",
     "question": "in nethack: set options",
    "date": 0,
    "box": 0
},
{
    "answer": "/",
     "question": "in nethack: tell what a map symbol represents",
    "date": 0,
    "box": 0
},
{
    "answer": "\",
     "question": "in nethack: display list of what's been discovered",
    "date": 0,
    "box": 0
},
{
    "answer": "v",
     "question": "in nethack: display version number",
    "date": 0,
    "box": 0
},
{
    "answer": "V",
     "question": "in nethack: display game history",
    "date": 0,
    "box": 0
},
{
    "answer": "^A",
     "question": "in nethack: redo the previous command (^A denotes the keystroke CTRL-A)",
    "date": 0,
    "box": 0
},
{
    "answer": "^R",
     "question": "in nethack: redraw the screen",
    "date": 0,
    "box": 0
},
{
    "answer": "^P",
     "question": "in nethack: repeat previous message (subsequent ^P's repeat earlier ones)",
    "date": 0,
    "box": 0
},
{
    "answer": "#",
     "question": "in nethack: introduce an extended command (#? for a list of them)",
    "date": 0,
    "box": 0
},
{
    "answer": "#?",
     "question": "in nethack: list extended commands",
    "date": 0,
    "box": 0
},
{
    "answer": "&",
     "question": "in nethack: describe the command a keystroke invokes",
    "date": 0,
    "box": 0
},

{
    "answer": "^D",
     "question": "in nethack: kick (a door, or something else)",
    "box": 0,
    "date": 0
},
{
    "answer": "^T",
     "question": "in nethack: teleport (if you can)",
    "box": 0,
    "date": 0
},
{
    "answer": "^X",
     "question": "in nethack: show your attributes",
    "box": 0,
    "date": 0
},
{
    "answer": "a",
    "question": "in nethack: apply or use a tool (pick-axe, key, camera, etc.)",
    "box": 0,
    "date": 0
},
{
    "answer": "A",
    "question": "in nethack: take off all armor",
    "box": 0,
    "date": 0
},
{
    "answer": "c",
    "question": "in nethack: close a door",
    "box": 0,
    "date": 0
},
{
    "answer": "C",
    "question": "in nethack: name a monster, an individual object, or a type of object",
    "box": 0,
    "date": 0
},
{
    "answer": "d",
    "question": "in nethack: drop an object.  d7a:  drop seven items of object 'a'",
    "box": 0,
    "date": 0
},
{
    "answer": "D",
    "question": "in nethack: drop selected types of objects",
    "box": 0,
    "date": 0
},
{
    "answer": "e",
    "question": "in nethack: eat something",
    "box": 0,
    "date": 0
},
{
    "answer": "E",
    "question": "in nethack: write a message in the dust on the floor  (E-  use fingers)",
    "box": 0,
    "date": 0
},
{
    "answer": "f",
    "question": "in nethack: fire ammunition from quiver",
    "box": 0,
    "date": 0
},
{
    "answer": "F",
    "question": "in nethack: followed by direction, fight a monster",
    "box": 0,
    "date": 0
},
{
    "answer": "i",
    "question": "in nethack: list your inventory (all objects you are carrying)",
    "box": 0,
    "date": 0
},
{
    "answer": "I",
    "question": "in nethack: list selected parts of your inventory",
    "box": 0,
    "date": 0
},
{
    "answer": "Iu",
    "question": "in nethack: list unpaid objects",
    "box": 0,
    "date": 0
},
{
    "answer": "Ix",
    "question": "in nethack: list unpaid but used up items",
    "box": 0,
    "date": 0
},

{
    "answer": "I$",
    "question": "in nethack: count your money",
    "box": 0,
    "date": 0
},
{
    "answer": "o",
    "question": "in nethack: open a door",
    "box": 0,
    "date": 0
},
{
    "answer": "p",
    "question": "in nethack: pay your bill (in a shop)",
    "box": 0,
    "date": 0
},
{
    "answer": "P",
    "question": "in nethack: put on an accessory (ring, amulet, etc)",
    "box": 0,
    "date": 0
},
{
    "answer": "q",
    "question": "in nethack: drink something (potion, water, etc)",
    "box": 0,
    "date": 0
},
{
    "answer": "Q",
    "question": "in nethack: select ammunition for quiver (use '#quit' to quit)",
    "box": 0,
    "date": 0
},
{
    "answer": "r",
    "question": "in nethack: read a scroll or spellbook",
    "box": 0,
    "date": 0
},
{
    "answer": "R",
    "question": "in nethack: remove an accessory (ring, amulet, etc)",
    "box": 0,
    "date": 0
},
{
    "answer": "s",
    "question": "in nethack: search for secret doors, hidden traps and monsters",
    "box": 0,
    "date": 0
},
{
    "answer": "t",
    "question": "in nethack: throw or shoot a weapon",
    "box": 0,
    "date": 0
},
{
    "answer": "T",
    "question": "in nethack: take off some armor",
    "box": 0,
    "date": 0
},
{
    "answer": "w",
    "question": "in nethack: wield a weapon  (w-  wield nothing)",
    "box": 0,
    "date": 0
},
{
    "answer": "W",
    "question": "in nethack: put on some armor",
    "box": 0,
    "date": 0
},
{
    "answer": "x",
    "question": "in nethack: swap wielded and secondary weapons",
    "box": 0,
    "date": 0
},
{
    "answer": "X",
    "question": "in nethack: toggle two-weapon combat (use '#explore' for explore mode)",
    "box": 0,
    "date": 0
},
{
    "answer": "z",
    "question": "in nethack: zap a wand  (use y instead of z if number_pad is -1)",
    "box": 0,
    "date": 0
},
{
    "answer": "Z",
    "question": "in nethack: cast a spell  (use Y instead of Z if number_pad is -1)",
    "box": 0,
    "date": 0
},
{
    "answer": "<",
    "question": "in nethack: go up the stairs",
    "box": 0,
    "date": 0
},
{
    "answer": ">",
    "question": "in nethack: go down the stairs",
    "box": 0,
    "date": 0
},
{
    "answer": "^",
    "question": "in nethack: identify a previously found trap",
    "box": 0,
    "date": 0
},
{
    "answer": "),[,=,",(",
    "question": "in nethack: ask for current items of specified symbol in use",
    "box": 0,
    "date": 0
},
{
    "answer": "*",
    "question": "in nethack: ask for combination of ),[,=,",( all at once",
    "box": 0,
    "date": 0
},
{
    "answer": "$",
    "question": "in nethack: count your gold",
    "box": 0,
    "date": 0
},
{
    "answer": "+",
    "question": "in nethack: list the spells you know; also rearrange them if desired",
    "box": 0,
    "date": 0
},
{
    "answer": "`",
    "question": "in nethack: display known items for one class of objects",
    "box": 0,
    "date": 0
},
{
    "answer": "_",
    "question": "in nethack: move via a shortest-path algorithm to a point on the map",
    "box": 0,
    "date": 0
},
{
    "answer": ".",
    "question": "in nethack: wait a moment",
    "box": 0,
    "date": 0
},
{
    "answer": ",",
    "question": "in nethack: pick up all you can carry",
    "box": 0,
    "date": 0
},
{
    "answer": "@",
    "question": "in nethack: toggle "pickup" (auto pickup) option on and off",
    "box": 0,
    "date": 0
},
{
    "answer": ":",
    "question": "in nethack: look at what is here",
    "box": 0,
    "date": 0
},
{
    "answer": ";",
    "question": "in nethack: look at what is somewhere else by selecting a map symbol",
    "box": 0,
    "date": 0
},

